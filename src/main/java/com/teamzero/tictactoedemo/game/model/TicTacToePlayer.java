package com.teamzero.tictactoedemo.game.model;

import java.util.Objects;

import com.teamzero.tictactoedemo.model.Player;
import com.teamzero.tictactoedemo.model.PlayerState;

public class TicTacToePlayer extends Player {

	private Mark mark;
	
	public TicTacToePlayer() { }
	
	public TicTacToePlayer(int id, String login, String password, Mark mark) {
		super(id, login, password);
		setMark(mark);
	}
	
	public Mark getMark() {
		return mark;
	}
	
	public void setMark(Mark mark) {
		this.mark = mark;
	}
	
	@Override
	public int hashCode( ) {
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		TicTacToePlayer other = (TicTacToePlayer) obj;
		return Objects.equals(getId(), other.getId()) && getLogin() == other.getLogin();
	}
	
	@Override
	public String toString() {
		return "TicTacToePlayer [mark=" + getMark() + ", score=" + getScore() + ", state=" + getState()
				+ ", id=" + getId() + ", login=" + getLogin() + "]";
	}
}
