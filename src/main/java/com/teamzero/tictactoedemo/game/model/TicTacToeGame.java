package com.teamzero.tictactoedemo.game.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.teamzero.tictactoedemo.model.Game;
import com.teamzero.tictactoedemo.model.GameState;
import com.teamzero.tictactoedemo.model.PlayerState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;

public class TicTacToeGame extends Game<TicTacToePlayer> {
	
	private final Logger logger = LoggerFactory.getLogger(TicTacToeGame.class);
	
	Board board = new Board();
	
	private TicTacToePlayer playerOne;
	private TicTacToePlayer playerTwo;
	
	private boolean isPlayerOneMoveFirst = true;
	private boolean isCurrentPlayerMove = true;
	
	public TicTacToeGame() { }
	
	public TicTacToeGame(TicTacToePlayer playerX, TicTacToePlayer playerO) {
		setPlayers(playerX, playerO);
		setState(GameState.PLAYING);
	}
	
	public boolean move(int position) {
		return move(position, currentPlayer().getMark());
	}
	
	public boolean move(int position, Mark mark) {
		
		if (getState().isPlaying() && 
			(currentPlayer().getMark() == mark) &&
			board.setIfEmpty(position, mark)) {
			
			logger.info("Player {} move on {}", currentPlayer().getMark(), position);
			
			if (board.isWinCombination()) {
				logger.info("Player {} win", currentPlayer().getMark());
				currentPlayer().setState(PlayerState.WIN);
				nextPlayer().setState(PlayerState.LOSS);
				setState(GameState.FINISHED);		
				
			} else if(board.isFull()) {
				logger.info("Draw");
				playerOne.setState(PlayerState.DRAW);
				playerTwo.setState(PlayerState.DRAW);
				setState(GameState.FINISHED);
			} 
			else 
				isCurrentPlayerMove = !isCurrentPlayerMove;
			
			return true;
		}
		return false;
	}
	
	public void randomMove() {
		
		logger.info("PC is move");
		move(board.getRandomAvailable(), currentPlayer().getMark());
	}
	
	public Board board() {
		return board;
	}
	
	public void swapMarks() {
		
		playerOne.setMark(playerTwo.getMark());
		playerTwo.setMark(playerOne.getMark() == Mark.X ? Mark.O : Mark.X);
	}
	
	public void setPlayers(TicTacToePlayer playerX, TicTacToePlayer playerO) {
		
		playerOne = playerX;
		playerOne.setMark(Mark.X);
		
		playerTwo = playerO;
		playerTwo.setMark(Mark.O);
	}
	
	public boolean isPlayerMoveFirst() {
		return isPlayerOneMoveFirst;
	}
		
	@Override
	public boolean setPlayers(Collection<TicTacToePlayer> players) {
		
		if (players.size() < 2)
			return false;
		
		Iterator<TicTacToePlayer> playerIterator = players.iterator();
		setPlayers(playerIterator.next(), playerIterator.next());
		
		return true;
	}
	
	@Override
	public TicTacToePlayer currentPlayer() {
		return isCurrentPlayerMove ? playerOne : playerTwo;
	}

	@Override
	public TicTacToePlayer nextPlayer() {
		return isCurrentPlayerMove ? playerTwo : playerOne;
	}

	@Override
	public Collection<TicTacToePlayer> players() {
		return new ArrayList<>() {{ add(playerOne); add(playerTwo); }};
	}
	
	@Override
	public void restart() {
		
		logger.info("Restarting");
		
		setState(GameState.PLAYING);
		isCurrentPlayerMove = isPlayerOneMoveFirst;
		playerOne.setState(PlayerState.PLAYING);
		playerTwo.setState(PlayerState.PLAYING);
		board.clear();
	}
	
	public void restart(boolean isPlayerOneMoveFirst) {
		this.isPlayerOneMoveFirst = isPlayerOneMoveFirst;
		restart();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		TicTacToeGame other = (TicTacToeGame) obj;
		return Objects.equals(getId(), other.getId());
	}

	@Override
	public String toString() {
		return "TicTacToeGame [getId()=" + getId() + "]";
	}
}
