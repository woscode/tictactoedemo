package com.teamzero.tictactoedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TictactoeDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TictactoeDemoApplication.class, args);
	}
}
