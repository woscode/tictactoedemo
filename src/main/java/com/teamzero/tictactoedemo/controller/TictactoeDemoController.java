package com.teamzero.tictactoedemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.teamzero.tictactoedemo.game.model.Mark;
import com.teamzero.tictactoedemo.game.model.TicTacToeGame;
import com.teamzero.tictactoedemo.game.model.TicTacToePlayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
@SessionAttributes("game")
@RequestMapping("/")
public class TictactoeDemoController {
	
	private static final Logger logger = LoggerFactory.getLogger(TictactoeDemoController.class);
	
	
	@GetMapping("/")
	public String tictactoe(@ModelAttribute("game") TicTacToeGame game) {
		return "tictactoegame";
	}
	
	@PostMapping("/")
	public String tictactoeMove(
			@ModelAttribute("game") TicTacToeGame game,
			@RequestParam("square_id") String squareId, 
			@RequestParam(value="restart", required=false, defaultValue="false") boolean restart,
			@RequestParam(value = "player_move_first", required = false) boolean playerMoveFirst) {
		if (restart) {
			logger.info("Is player move first? : {}", playerMoveFirst);
			game.restart(playerMoveFirst);
			if (!game.isPlayerMoveFirst())
				game.move(4);
		} else {
			logger.info("Player move on {}", squareId);
			game.move(Integer.valueOf(squareId));
			game.randomMove();
			// game.move(Integer.valueOf(squareId), Mark.O);
		}
		return "tictactoegame";
	}

	public String goodbye(SessionStatus status) {
	    status.setComplete();
	    return "tictactoegame";
	 }

	
	@ModelAttribute("game")
	private TicTacToeGame tictactoeGame() {
		logger.info("Creating game model...");
		return new TicTacToeGame (
				new TicTacToePlayer(0, "GUEST_1", "_", Mark.X), 
				new TicTacToePlayer(1, "GUEST_2", "_", Mark.O)
			);
	}
}
