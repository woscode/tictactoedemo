package com.teamzero.tictactoedemo.model;

import java.time.LocalDateTime;

/**
 * @Unused
 */
public class GameSession {
	
	private String address;
	private Game<Player> game;
	
	private LocalDateTime starTime;
	private LocalDateTime endTime;
	
	public String create(Game<Player> game) {
		starTime = LocalDateTime.now();
		this.game = game;
		return address;
	}
	
	public void stop( ) {
		endTime = LocalDateTime.now();
	}
}
