package com.teamzero.tictactoedemo.model;

import java.util.Collection;
import java.util.Objects;


public abstract class Game <P extends Player> {
	
	private int id;
	
	private GameState state = GameState.NEW;

	protected Game() { }
	
	protected Game(int id) {
		setId(id);
	}
	
	/**
	 * Go to the next player
	 * @return the next player
	 * */
	public abstract P nextPlayer();
	
	public abstract P currentPlayer();
	
	public abstract Collection<P> players();
	public abstract boolean setPlayers(Collection<P> players);

	public abstract void restart();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public GameState getState() {
		return state;
	}
	
	public void setState(GameState state) {
		this.state = state;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		return Objects.equals(id, other.id);
	}
}
