package com.teamzero.tictactoedemo.model;

import java.util.Objects;

public class Player extends User implements Comparable<Player> {
	
	private PlayerState state;
	private int score = 0;
	
	public Player() {}
	
	public Player(int id, String login, String password) {
		super(id, login, password);
		setState(PlayerState.PLAYING);
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public PlayerState getState() {
		return state;
	}
	
	public void setState(PlayerState state) {
		this.state = state;
		if (this.state.isWin())
			score++;
	}
	
	public void reset() {
		state = PlayerState.PLAYING;
		score = 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		return Objects.equals(getId(), other.getId()) && getLogin() == other.getLogin();
	}
	
	@Override
	public int hashCode( ) {
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		return "Player [state=" + state + ", score=" + score + ", getId()=" + getId() + ", getLogin()=" + getLogin()
				+ "]";
	}

	@Override
	public int compareTo(Player other) {
		return Integer.compare(score, other.score);
	}
}
