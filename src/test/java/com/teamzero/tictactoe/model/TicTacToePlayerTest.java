package com.teamzero.tictactoe.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.teamzero.tictactoedemo.game.model.Mark;
import com.teamzero.tictactoedemo.game.model.TicTacToePlayer;
import com.teamzero.tictactoedemo.model.PlayerState;

public class TicTacToePlayerTest {

	@Test
	void init() {
		
		TicTacToePlayer player = new TicTacToePlayer();
		
		assertEquals(0, player.getId());
		assertNull(player.getLogin());
		assertNull(player.getPassword());
		assertNull(player.getMark());
		assertNull(player.getState());
				
		player = new TicTacToePlayer(1, "login1", "password1", Mark.X);
		
		assertEquals(1, player.getId());
		assertEquals("login1", player.getLogin());
		assertEquals("password1", player.getPassword());
		assertEquals(Mark.X, player.getMark());
		assertEquals(PlayerState.PLAYING, player.getState());
	}
	
	@Test
	void equals() {
		
		TicTacToePlayer playerOne = new TicTacToePlayer(1, "login1", "password1", Mark.X);
		TicTacToePlayer playerTwo = new TicTacToePlayer(2, "login2", "password2", Mark.O);
		TicTacToePlayer playerThree = new TicTacToePlayer(1, "login1", "password3", Mark.O);
		
		assertTrue(playerOne.equals(playerOne));
		assertTrue(playerOne.equals(playerThree));
		
		assertFalse(playerOne.equals(playerTwo));
		
		assertEquals(playerOne.hashCode(), playerOne.hashCode());
		assertEquals(playerOne.hashCode(), playerThree.hashCode());
		
		assertNotEquals(playerOne.hashCode(), playerTwo.hashCode());
	}
	
	@Test
	void score() {
		
		TicTacToePlayer playerOne = new TicTacToePlayer(1, "login1", "password1", Mark.X);
		TicTacToePlayer playerTwo = new TicTacToePlayer(2, "login2", "password2", Mark.O);
		
		playerOne.setState(PlayerState.DRAW);
		assertEquals(0, playerOne.getScore());
		
		playerOne.setState(PlayerState.LOSS);
		assertEquals(0, playerOne.getScore());
		
		playerOne.setState(PlayerState.WIN);
		assertEquals(PlayerState.WIN, playerOne.getState());
		assertEquals(1, playerOne.getScore());
		
		playerOne.setScore(5);
		assertEquals(5, playerOne.getScore());
	}
}
