package com.teamzero.tictactoe.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.teamzero.tictactoedemo.game.model.Mark;
import com.teamzero.tictactoedemo.game.model.TicTacToeGame;
import com.teamzero.tictactoedemo.game.model.TicTacToePlayer;
import com.teamzero.tictactoedemo.model.GameState;

class TicTacToeGameTest {
	
	private static List<TicTacToePlayer> players;
	
	@BeforeAll
	static void initAll() {

		players = new ArrayList<>();
		players.add(new TicTacToePlayer(0, "GUEST_0", "", Mark.O));
		players.add(new TicTacToePlayer(1, "login1", "password1", Mark.X)); // 1, "login1", "password1"
		players.add(new TicTacToePlayer(2, "login2", "password2", Mark.O));
		players.add(new TicTacToePlayer(0, "GUEST_1", "", Mark.X));
	}
	
	@Test
	void init() {
		TicTacToeGame game = new TicTacToeGame();
		
		assertEquals(GameState.NEW, game.getState());
		assertEquals(0, game.getId());
		assertNotNull(game.board());
		assertNotNull(game.players());
		assertNull(game.currentPlayer());
		assertFalse(game.players().isEmpty());
	}
	
	@Test
	void start() {
		TicTacToeGame game = new TicTacToeGame(players.get(1), players.get(2));
				
		assertFalse(game.board().isFull());
		assertEquals(GameState.PLAYING, game.getState());
		assertEquals(players.get(1), game.currentPlayer());
		
		game.restart();
		assertFalse(game.board().isFull());
		assertEquals(GameState.PLAYING, game.getState());
		assertEquals(players.get(1), game.currentPlayer());
	}
	
	@Test
	void players() {
		TicTacToeGame game = new TicTacToeGame(players.get(1), players.get(2));
		assertEquals(players.get(1), game.currentPlayer());
		assertEquals(players.get(2), game.nextPlayer());
	}
	
	@Test
	void move() {
		TicTacToeGame game = new TicTacToeGame(players.get(1), players.get(2));
		
		assertTrue(game.move(1,Mark.X));
		
		assertFalse(game.move(1,Mark.O));
		assertFalse(game.move(8,Mark.X));
		
		assertTrue(game.move(2,Mark.O));
		assertTrue(game.move(4,Mark.X));
		
		assertTrue(game.move(5,Mark.O));
		assertTrue(game.move(8,Mark.X));
	}
}
