package com.teamzero.tictactoe.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.teamzero.tictactoedemo.game.model.Mark;
import com.teamzero.tictactoedemo.game.model.Square;

class SquareTest {

	@Test
	void init() {
		
		Square square = new Square(0);
		assertTrue(square.isEmpty());
		assertEquals(Mark.EMPTY, square.getMark());
		
		square.setMark(Mark.X);
		assertFalse(square.isEmpty());
	}
}
